﻿Imports System.ComponentModel
Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Text.RegularExpressions
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Class Form1
    Dim loginurl = "https://app.pluralsight.com/id/"
    Dim p As Pluralsight
    Dim Folder As String = Application.StartupPath
    Public WithEvents webClient As WebClient

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim c As RootCourse = p.ParseCourse(Pluralsight.GetCourseID(TextBox3.Text))
        For Each models As [Module] In c.payload.course.modules
            Dim Listgroup As New ListViewGroup(models.title)
            ListView1.Groups.Add(Listgroup)
            For Each clip As Clip In models.clips
                Dim cl As New ClipModel
                cl.author = models.author
                cl.moduleName = models.name
                cl.courseName = c.payload.course.name
                cl.clipIndex = clip.index
                cl.moduleTitle = clip.moduleTitle
                cl.clipTitle = clip.title
                cl.courseTitle = c.payload.course.title
                Dim ListItem As New ListViewItem(clip.title)
                ListItem.Checked = True
                ListItem.SubItems.Add(clip.formattedDuration)
                ListItem.SubItems.Add(clip.id)
                ListItem.Group = Listgroup
                ListItem.Tag = cl
                ListView1.Items.Add(ListItem)
            Next
        Next
    End Sub

    Private Async Sub Button1_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim i As Integer
        Dim count = ListView1.Items.Count
        Dim stoptime As New Stopwatch
        For Each liv As ListViewItem In ListView1.Items
            If liv.Checked = True Then
                If i = 0 Then i = 1

                Dim Clip As ClipModel = DirectCast(liv.Tag, ClipModel)
                Dim filePath As String = GetFileClip(clip, i)
                Label5.Text = String.Format("{0}/{1}", i, count)
                If Not File.Exists(filePath) Then
                    stoptime.Start()
                    Dim uri As String = p.GetClipUrl(Clip)
                    setFolderModule(Clip)
                    Label6.Text = Clip.clipTitle
                    Application.DoEvents()
                    webClient = New WebClient
                    Await webClient.DownloadFileTaskAsync(New Uri(uri), filePath)
                    If (stoptime.ElapsedMilliseconds < 1000) Then
                        Threading.Thread.Sleep(1000)
                        Label5.Text = "wait .."
                    End If
                    stoptime.Restart()
                End If
                i += 1
            End If
        Next

    End Sub
    Private Function VaildName(UserInput As String) As String
        Return Regex.Replace(UserInput, "[\/<>\[\:\]\{\}\?]", "-")
    End Function
    Public Function GetFileClip(clip As ClipModel, number As Integer)

        Return Path.Combine(Folder, VaildName(clip.courseTitle), VaildName(clip.moduleTitle), number & "- " & VaildName(clip.clipTitle) & "." & clip.mediaType)
    End Function
    Public Sub setFolderModule(clip As ClipModel)

        Dim mPath As String = Path.Combine(Folder, VaildName(clip.courseTitle), VaildName(clip.moduleTitle))
        If Not Directory.Exists(mPath) Then Directory.CreateDirectory(mPath)
    End Sub
    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        p = New Pluralsight
        Try
            If p.Login(TextBox1.Text, TextBox2.Text) Then
                Label3.Visible = True
            Else
                Label4.Visible = True
            End If

        Catch ex As Exception
            Label4.Visible = True
        End Try
    End Sub


    Private Sub webClient_DownloadFileCompleted(sender As Object, e As AsyncCompletedEventArgs) Handles webClient.DownloadFileCompleted
        ProgressBar1.Value = 0
    End Sub

    Private Sub webClient_DownloadProgressChanged(sender As Object, e As DownloadProgressChangedEventArgs) Handles webClient.DownloadProgressChanged
        Label7.Text = String.Format("{0} / {1}", FormatSize(e.BytesReceived), FormatSize(e.TotalBytesToReceive))
        Label8.Text = String.Format("{0}%", e.ProgressPercentage.ToString)
        ProgressBar1.Value = e.ProgressPercentage
    End Sub
    Private Function FormatSize(TheSize As ULong) As String
        Try
            Dim DoubleBytes As Double
            Select Case TheSize
                Case Is >= 1099511627776
                    DoubleBytes = CDbl(TheSize / 1099511627776)
                    Return FormatNumber(DoubleBytes, 2) & " TB"
                Case 1073741824 To 1099511627775
                    DoubleBytes = CDbl(TheSize / 1073741824) 'GB
                    Return FormatNumber(DoubleBytes, 2) & " GB"
                Case 1048576 To 1073741823
                    DoubleBytes = CDbl(TheSize / 1048576) 'MB
                    Return FormatNumber(DoubleBytes, 2) & " MB"
                Case 1024 To 1048575
                    DoubleBytes = CDbl(TheSize / 1024) 'KB
                    Return FormatNumber(DoubleBytes, 2) & " KB"
                Case 0 To 1023
                    DoubleBytes = TheSize ' bytes
                    Return FormatNumber(DoubleBytes, 2) & " bytes"
                Case Else
                    Return ""
            End Select
        Catch
            Return ""
        End Try

    End Function
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub GroupBox2_Enter(sender As Object, e As EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If webClient IsNot Nothing Then webClient.Dispose()
        Application.Exit()
        End
    End Sub

    Private Sub Form1_Activated(sender As Object, e As EventArgs) Handles Me.Activated

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        ListView1.Items.Clear()
    End Sub
End Class
