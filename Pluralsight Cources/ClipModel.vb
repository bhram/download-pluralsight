﻿Public Class ClipModel
    Public Property author As String
    Public Property clipIndex As Integer
    Public Property courseName As String
    Public Property moduleName As String
    Public Property includeCaptions As Boolean = False
    Public Property locale As String = "en"
    Public Property mediaType As String = "mp4"
    Public Property quality As String = "1024x768"
    Public Property courseTitle As String
    Public Property moduleTitle As String
    Public Property clipTitle As String
End Class
