﻿Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Text.RegularExpressions
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Class Pluralsight
    Public cookie As New CookieContainer
    Private LoginUrl = "https://app.pluralsight.com/id/"
    Private FetchCourse = "https://app.pluralsight.com/player/functions/rpc"

    Public Function Login(username As String, password As String) As Boolean
        Dim Data = "RedirectUrl=&Username=" & username & "&Password=" & password & "&ShowCaptcha=False&ReCaptchaSiteKey=6LeVIgoTAAAAAIhx_TOwDWIXecbvzcWyjQDbXsaV"
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(Data)
        Dim webReq As HttpWebRequest = DirectCast(HttpWebRequest.Create(LoginUrl), HttpWebRequest)
        With webReq
            .Method = "POST"
            .UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0"
            .ContentType = "application/x-www-form-urlencoded"
            .Headers.Add("Upgrade-Insecure-Requests: 1")
            .ContentLength = byteArray.Length
            .CookieContainer = cookie
            .ContentType = "application/x-www-form-urlencoded"
            .Referer = "https://app.pluralsight.com/"
            .Host = "app.pluralsight.com"
            .KeepAlive = True
        End With
        Dim dataStream As Stream = webReq.GetRequestStream()
        dataStream.Write(byteArray, 0, byteArray.Length)
        dataStream.Close()
        webReq.GetResponse()
        Return isLogin()
    End Function
    Public Function ParseCourse(courseID As String) As RootCourse
        Dim Data = "{""fn"":""bootstrapPlayer"",""payload"":{""courseId"":""" & courseID & """}}"
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(Data)
        Dim webReq As HttpWebRequest = DirectCast(HttpWebRequest.Create(FetchCourse), HttpWebRequest)
        With webReq
            .Method = "POST"
            .UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0"
            .ContentType = "application/x-www-form-urlencoded"
            .Headers.Add("Upgrade-Insecure-Requests: 1")
            .ContentLength = byteArray.Length
            .CookieContainer = cookie
            .ContentType = "application/json;charset=UTF-8"
            .Referer = "https://app.pluralsight.com/"
            .Host = "app.pluralsight.com"
            .KeepAlive = True
        End With
        Dim dataStream As Stream = webReq.GetRequestStream()
        dataStream.Write(byteArray, 0, byteArray.Length)
        dataStream.Close()
        Dim response As HttpWebResponse = DirectCast(webReq.GetResponse(), HttpWebResponse)
        Dim read As New StreamReader(response.GetResponseStream())
        Return JsonConvert.DeserializeObject(Of RootCourse)(read.ReadToEnd)
    End Function
    Public Function GetClipUrl(clip As ClipModel) As String
        Dim apiClip As String = "https://app.pluralsight.com/video/clips/viewclip"
        Dim data = JsonConvert.SerializeObject(clip)
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(data)
        Dim webReq As HttpWebRequest = DirectCast(HttpWebRequest.Create(apiClip), HttpWebRequest)
        With webReq
            .Method = "POST"
            .UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0"
            .ContentLength = byteArray.Length
            .CookieContainer = cookie
            .ContentType = "application/json;charset=UTF-8"
            .Referer = "https://app.pluralsight.com/"
            .Host = "app.pluralsight.com"
            .KeepAlive = True
        End With
        Dim dataStream As Stream = webReq.GetRequestStream()
        dataStream.Write(byteArray, 0, byteArray.Length)
        dataStream.Close()
        Dim response As HttpWebResponse = DirectCast(webReq.GetResponse(), HttpWebResponse)

        Dim read As New StreamReader(response.GetResponseStream())
        Return JObject.Parse(read.ReadToEnd).GetValue("urls")(1).Item("url")
    End Function

    Public Shared Function GetCourseID(url As String) As String
        Dim regex As New Regex("courses?[\/=]([\w-]+)")
        Dim match As Match = regex.Match(url)
        If match.Success Then Return match.Groups(1).Value
        Return Nothing
    End Function

    Public Function isLogin()
        Return cookie.Count > 1
    End Function
End Class
