﻿Public Class AssessmentsHomeIntegration
    Public Property id As String
    Public Property displayName As String
    Public Property description As String
    Public Property isAvailableForOptIn As Boolean
    Public Property isEnabledForUser As Boolean
    Public Property canToggle As Boolean
End Class

Public Class LearnerHome2
    Public Property id As String
    Public Property displayName As String
    Public Property description As String
    Public Property isAvailableForOptIn As Boolean
    Public Property isEnabledForUser As Boolean
    Public Property canToggle As Boolean
End Class

Public Class LearnerSliceHome
    Public Property id As String
    Public Property displayName As String
    Public Property description As Object
    Public Property isAvailableForOptIn As Boolean
    Public Property isEnabledForUser As Boolean
    Public Property canToggle As Boolean
End Class



Public Class Subscription
    Public Property hasIndividualSubscription As Boolean
    Public Property trial As Boolean
    Public Property expirationDateUtc As DateTime
End Class

Public Class Profile
    Public Property authed As Boolean
    Public Property firstName As String
    Public Property lastName As String
    Public Property email As String
    Public Property username As String
    Public Property userHandle As String
    Public Property claims As Object()
    Public Property plan As String
    Public Property subscription As Subscription
End Class

Public Class Clip
    Public Property authorized As Boolean
    Public Property duration As Integer
    Public Property formattedDuration As String
    Public Property index As Integer
    Public Property moduleIndex As Integer
    Public Property name As String
    Public Property title As String
    Public Property watched As Boolean
    Public Property id As String
    Public Property moduleTitle As String
End Class

Public Class [Module]
    Public Property name As String
    Public Property title As String
    Public Property duration As Integer
    Public Property formattedDuration As String
    Public Property author As String
    Public Property clips As Clip()
    Public Property authorized As Boolean
End Class

Public Class TranslationLanguage
    Public Property code As String
    Public Property name As String
End Class

Public Class Course
    Public Property title As String
    Public Property name As String
    Public Property modules As [Module]()
    Public Property courseHasCaptions As Boolean
    Public Property supportsWideScreenVideoFormats As Boolean
    Public Property translationLanguages As TranslationLanguage()
    Public Property timestamp As DateTime
End Class

Public Class Payload
    Public Property profile As Profile
    Public Property course As Course
End Class

Public Class Trace
    Public Property service As String
    Public Property version As String
    Public Property latency As Integer
    Public Property fn As String
End Class

Public Class RootCourse
    Public Property success As Boolean
    Public Property payload As Payload
    Public Property trace As Trace()
End Class
